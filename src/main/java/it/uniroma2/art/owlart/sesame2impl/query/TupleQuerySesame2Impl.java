/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.query;

import java.io.OutputStream;

import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.query.resultio.TupleQueryResultWriter;
import org.openrdf.query.resultio.binary.BinaryQueryResultWriter;
import org.openrdf.query.resultio.sparqljson.SPARQLResultsJSONWriter;
import org.openrdf.query.resultio.sparqlxml.SPARQLResultsXMLWriter;

import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWritingFormat;

public class TupleQuerySesame2Impl extends QuerySesame2Impl implements TupleQuery {

	public TupleQuerySesame2Impl(org.openrdf.query.TupleQuery query) {
		super(query);
	}

	public TupleBindingsIterator evaluate(boolean infer) throws QueryEvaluationException {
		try {
			query.setIncludeInferred(infer);
			return new TupleBindingsIteratorSesame2Impl(((org.openrdf.query.TupleQuery)query).evaluate());
		} catch (org.openrdf.query.QueryEvaluationException e) {
			throw new QueryEvaluationException(e);
		}
	}

	public void evaluate(boolean infer, TupleBindingsWritingFormat format, OutputStream os)
			throws QueryEvaluationException, TupleBindingsWriterException {
		query.setIncludeInferred(infer);
		try {
			((org.openrdf.query.TupleQuery)query).evaluate(getWriter(format, os));
		} catch (org.openrdf.query.QueryEvaluationException e) {
			throw new QueryEvaluationException(e);
		} catch (TupleQueryResultHandlerException e) {
			throw new TupleBindingsWriterException(e);
		}
	}

	
	private TupleQueryResultWriter getWriter(TupleBindingsWritingFormat format, OutputStream os) throws QueryEvaluationException {
		if (format==TupleBindingsWritingFormat.XML)
			return new SPARQLResultsXMLWriter(os);
		if (format==TupleBindingsWritingFormat.JSON)
			return new SPARQLResultsJSONWriter(os);
		if (format==TupleBindingsWritingFormat.BIN)
			return new BinaryQueryResultWriter(os);
		throw
			new QueryEvaluationException("unknown export format");
	}
	
	
}
