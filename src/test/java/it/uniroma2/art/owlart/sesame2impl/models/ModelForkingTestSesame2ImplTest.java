package it.uniroma2.art.owlart.sesame2impl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.ModelForkingTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

public class ModelForkingTestSesame2ImplTest extends ModelForkingTest {

	@BeforeClass
	public static void initialize() throws Exception {
		initializeTest(new ARTModelFactorySesame2Impl());
	}
	
}
