package it.uniroma2.art.owlart.sesame2impl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;

public class Sesame2NativeModelConfiguration extends Sesame2DirectAccessModelConfiguration implements
		Sesame2ModelConfiguration, PersistenceModelConfiguration {

	@ModelConfigurationParameter(description = "Specifies whether updates should be synced to disk forcefully; defaults to false")
	public Boolean forceSync = false;

	@ModelConfigurationParameter(description = "specifies the triple indexes to be created for optimizing"
			+ "query resolution; see: http://www.openrdf.org/doc/sesame2/users/ch07.html#section-native-store-config; defaults to spoc, posc")
	public String tripleIndexes = "spoc, posc";

	public Sesame2NativeModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "native store / persistent";
	}

	public boolean isPersistent() {
		return true;
	}
}
