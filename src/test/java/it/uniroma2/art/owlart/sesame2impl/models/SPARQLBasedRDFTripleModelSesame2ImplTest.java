package it.uniroma2.art.owlart.sesame2impl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.SPARQLBasedRDFTripleModelImplTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

public class SPARQLBasedRDFTripleModelSesame2ImplTest extends SPARQLBasedRDFTripleModelImplTest {

	@BeforeClass
	public static void initialize() {
		SPARQLBasedRDFTripleModelImplTest.initializeModelFactory(new ARTModelFactorySesame2Impl());		
	}
	
	
	
}
