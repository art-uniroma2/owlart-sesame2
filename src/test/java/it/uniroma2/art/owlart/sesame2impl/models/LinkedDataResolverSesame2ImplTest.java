package it.uniroma2.art.owlart.sesame2impl.models;

import it.uniroma2.art.owlart.models.LinkedDataResolverTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

import org.junit.BeforeClass;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class LinkedDataResolverSesame2ImplTest extends LinkedDataResolverTest {
	
	@BeforeClass
	public static void loadLinkedDataResolver() throws Exception {
		initializeTest(new ARTModelFactorySesame2Impl());
	}
}
