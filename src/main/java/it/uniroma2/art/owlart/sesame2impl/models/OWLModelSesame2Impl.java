/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.models;

import org.openrdf.repository.RepositoryConnection;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.TransactionBasedModel;
import it.uniroma2.art.owlart.models.impl.OWLModelImpl;
import it.uniroma2.art.owlart.models.impl.ObjectsOfStatementsIterator;
import it.uniroma2.art.owlart.models.impl.ResourceIteratorWrappingNodeIterator;
import it.uniroma2.art.owlart.models.impl.SubjectsOfStatementsIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorWrappingNodeIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorWrappingResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.sesame2impl.vocabulary.SESAME;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;

public class OWLModelSesame2Impl extends OWLModelImpl implements RDFModelSesame2, DirectReasoning,
		TransactionBasedModel {

	boolean directSupported;

	public OWLModelSesame2Impl(BaseRDFModelSesame2Impl baseRep) throws VocabularyInitializationException {
		super(baseRep);
		SESAME.Res.initialize(this);
		directSupported = baseRep.directTypeReasoning;
	}

	public RepositoryConnection getSesame2RepositoryConnection() {
		return ((BaseRDFModelSesame2Impl) baseRep).getSesame2RepositoryConnection();
	}

	public ARTResourceIterator listDirectInstances(ARTResource type, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, SESAME.Res.DIRECTTYPE,
					type, true, contexts));
		return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, RDF.Res.TYPE, type, false,
				contexts));
	}

	public ARTResourceIterator listDirectTypes(ARTResource res, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return RDFIterators.toResourceIterator(new ObjectsOfStatementsIterator(listStatements(res,
					SESAME.Res.DIRECTTYPE, NodeFilters.ANY, true, contexts)));
		return RDFIterators.toResourceIterator(new ObjectsOfStatementsIterator(listStatements(res,
				RDF.Res.TYPE, NodeFilters.ANY, false, contexts)));
	}

	public ARTResourceIterator listDirectSuperClasses(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return new ResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(
					resource, SESAME.Res.DIRECTSUBCLASSOF, NodeFilters.ANY, true, contexts)));
		return new ResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(
				resource, RDFS.Res.SUBCLASSOF, NodeFilters.ANY, false, contexts)));
	}

	public ARTURIResourceIterator listDirectSuperProperties(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return new URIResourceIteratorWrappingNodeIterator(
					new ObjectsOfStatementsIterator(listStatements(resource, SESAME.Res.DIRECTSUBPROPERTYOF,
							NodeFilters.ANY, true, contexts)));
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(
				resource, RDFS.Res.SUBPROPERTYOF, NodeFilters.ANY, false, contexts)));
	}

	public ARTResourceIterator listDirectSubClasses(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY,
					SESAME.Res.DIRECTSUBCLASSOF, resource, true, contexts));
		return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, RDFS.Res.SUBCLASSOF,
				resource, false, contexts));
	}

	public ARTURIResourceIterator listDirectSubProperties(ARTURIResource resource, ARTResource... contexts)
			throws ModelAccessException {
		if (directSupported)
			return new URIResourceIteratorWrappingResourceIterator(
					new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY,
							SESAME.Res.DIRECTSUBPROPERTYOF, resource, true, contexts)));
		return new URIResourceIteratorWrappingResourceIterator(new SubjectsOfStatementsIterator(
				listStatements(NodeFilters.ANY, RDFS.Res.SUBPROPERTYOF, resource, false, contexts)));
	}

	public void commit() throws ModelUpdateException {
		((TransactionBasedModel) baseRep).commit();
	}

	public boolean isAutoCommit() throws ModelAccessException {
		return ((TransactionBasedModel) baseRep).isAutoCommit();
	}

	public void rollBack() throws ModelAccessException {
		((TransactionBasedModel) baseRep).rollBack();
	}

	public void setAutoCommit(boolean value) throws ModelUpdateException {
		((TransactionBasedModel) baseRep).setAutoCommit(value);
	}

	/****************************
	 *    FORKING OPERATIONS    *
	 ****************************/

	@Override
	public OWLModelSesame2Impl forkModel() throws ModelCreationException {
		if (this.getClass() != OWLModelSesame2Impl.class) {
			throw new IllegalStateException("The model class '" + this.getClass().getName() + "' seems not properly override forkModel()");
		}
		try {
			return new OWLModelSesame2Impl((BaseRDFModelSesame2Impl)baseRep.forkModel());
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}	
	}
}
