package it.uniroma2.art.owlart.sesame2impl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;
import it.uniroma2.art.owlart.models.conf.RemoteModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.RequiredConfigurationParameter;

public class Sesame2RemoteModelConfiguration extends RemoteModelConfigurationImpl implements
		Sesame2ModelConfiguration, PersistenceModelConfiguration {

	@ModelConfigurationParameter(description = "id of the sesame2 repository to be accessed")
	@RequiredConfigurationParameter
	public String repositoryId;

	@ModelConfigurationParameter(description = "set to 'false' if the inferred null context does not contain triples from other named graphs (e.g. as for OWLIM)")
	public boolean inferredNullContextContainsAllTriples = true;

	public Sesame2RemoteModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "remote access";
	}

	public boolean isPersistent() {
		return true;
	}
}
