package it.uniroma2.art.owlart.sesame2impl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.SPARQLConnectionTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

public class SPARQLConnectionSesame2ImplTest extends SPARQLConnectionTest {

	@BeforeClass
	public static void initialize() {
		SPARQLConnectionTest.initializeModelFactory(new ARTModelFactorySesame2Impl());
	}
	
	
	
}
