package it.uniroma2.art.owlart.sesame2impl.models.conf;

import org.openrdf.sail.memory.MemoryStore;

import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

/**
 * This interface has no methods, it is just a tag to inform the {@link ARTModelFactorySesame2Impl} that a the
 * repository which will be created is based on the Sesame2 {@link MemoryStore} sail implementation
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public interface Sesame2InMemoryModelConfiguration extends Sesame2ModelConfiguration {

}
