package it.uniroma2.art.owlart.sesame2impl.utilities.refactor;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.utilites.refactor.BaseURIRefactorTest;

public class BaseURIRefactorSesame2ImplTest extends BaseURIRefactorTest{

	@BeforeClass
	public static void loadRepository() throws Exception {
		BaseURIRefactorTest.initializeTest(new ARTModelFactorySesame2Impl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseURIRefactorTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
}
