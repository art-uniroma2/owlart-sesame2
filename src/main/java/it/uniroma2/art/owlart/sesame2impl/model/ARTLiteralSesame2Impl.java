/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.model;

import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTLiteralSesame2Impl extends ARTNodeSesame2Impl implements ARTLiteral {

	public ARTLiteralSesame2Impl(Literal lit) {
		super(lit);
	}
	
	/* (non-Javadoc)
	 * @see it.uniroma2.art.owlart.model.ARTLiteral#getDatatype()
	 */
	public ARTURIResource getDatatype() {
		URI dtype = ((Literal)node).getDatatype();
		if (dtype!=null)
			return new ARTURIResourceSesame2Impl(dtype);
		return null;		
	}

	public String getLabel() {
		return (((Literal)node).getLabel());
	}

	public String getLanguage() {
		return (((Literal)node).getLanguage());
	}
	
	public Literal getLiteral() {
		return (Literal)node;
	}

}
