/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.query;

import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.query.QueryLanguage;

import java.util.HashMap;

public class QueryLanguageConverter {

	private static HashMap<QueryLanguage, org.openrdf.query.QueryLanguage> conversionMap;

	static {
		conversionMap = new HashMap<QueryLanguage, org.openrdf.query.QueryLanguage>();
		conversionMap.put(QueryLanguage.SPARQL, org.openrdf.query.QueryLanguage.SPARQL);
	}

	public static org.openrdf.query.QueryLanguage convert(QueryLanguage ql) throws UnsupportedQueryLanguageException {
		org.openrdf.query.QueryLanguage output = conversionMap.get(ql);
		if (output == null)
			throw new UnsupportedQueryLanguageException("query language: " + ql
					+ " is not supported by current Sesame2 OWLArt Implementation");
		else
			return output;
	}


}
