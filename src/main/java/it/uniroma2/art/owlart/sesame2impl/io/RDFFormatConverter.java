/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.io;

import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;

import java.util.HashMap;

import org.openrdf.rio.RDFWriterFactory;

/**
 * this class provides static conversion methods between OWL ART supported RDF serialization formats and their
 * definitions in Sesame2 library
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class RDFFormatConverter {

	private static HashMap<RDFFormat, org.openrdf.rio.RDFFormat> conversionMap;
	private static HashMap<RDFFormat, org.openrdf.rio.RDFWriterFactory> writersMap;

	static {
		conversionMap = new HashMap<RDFFormat, org.openrdf.rio.RDFFormat>();
		conversionMap.put(RDFFormat.N3, org.openrdf.rio.RDFFormat.N3);
		conversionMap.put(RDFFormat.NTRIPLES, org.openrdf.rio.RDFFormat.NTRIPLES);
		conversionMap.put(RDFFormat.RDFXML, org.openrdf.rio.RDFFormat.RDFXML);
		conversionMap.put(RDFFormat.RDFXML_ABBREV, org.openrdf.rio.RDFFormat.RDFXML);
		conversionMap.put(RDFFormat.TRIG, org.openrdf.rio.RDFFormat.TRIG);
		conversionMap.put(RDFFormat.TRIX, org.openrdf.rio.RDFFormat.TRIX);
		conversionMap.put(RDFFormat.TURTLE, org.openrdf.rio.RDFFormat.TURTLE);
		conversionMap.put(RDFFormat.NQUADS, org.openrdf.rio.RDFFormat.NQUADS);

		writersMap = new HashMap<RDFFormat, RDFWriterFactory>();
		writersMap.put(RDFFormat.N3, new org.openrdf.rio.n3.N3WriterFactory());
		writersMap.put(RDFFormat.NTRIPLES, new org.openrdf.rio.ntriples.NTriplesWriterFactory());
		writersMap.put(RDFFormat.RDFXML, new org.openrdf.rio.rdfxml.RDFXMLWriterFactory());
		writersMap.put(RDFFormat.RDFXML_ABBREV, new org.openrdf.rio.rdfxml.util.RDFXMLPrettyWriterFactory());
		writersMap.put(RDFFormat.TRIG, new org.openrdf.rio.trig.TriGWriterFactory());
		writersMap.put(RDFFormat.TRIX, new org.openrdf.rio.trix.TriXWriterFactory());
		writersMap.put(RDFFormat.TURTLE, new org.openrdf.rio.turtle.TurtleWriterFactory());
		writersMap.put(RDFFormat.NQUADS, new org.openrdf.rio.nquads.NQuadsWriterFactory());
	}

	/**
	 * gets the Sesame2 RDF Format from its OWL ART counterpart 
	 * 
	 * @param format
	 * @return
	 * @throws UnsupportedRDFFormatException
	 */
	public static org.openrdf.rio.RDFFormat convert(RDFFormat format) throws UnsupportedRDFFormatException {
		org.openrdf.rio.RDFFormat output = conversionMap.get(format);
		if (output == null)
			throw new UnsupportedRDFFormatException("format: " + format
					+ " is not supported by current Sesame2 OWLArt Implementation");
		else
			return output;
	}

	/**
	 * gets a Sesame2 WriterFactory from OWL ART {@link RDFFormat}
	 * 
	 * @param format
	 * @return
	 * @throws UnsupportedRDFFormatException
	 */
	public static RDFWriterFactory getWriterFactory(RDFFormat format) throws UnsupportedRDFFormatException {
		RDFWriterFactory wFact = writersMap.get(format);
		if (wFact == null)
			throw new UnsupportedRDFFormatException("format: " + format
					+ " is not supported by current Sesame2 OWLArt Implementation");
		else
			return wFact;
	}

}
