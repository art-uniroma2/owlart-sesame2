package it.uniroma2.art.owlart.sesame2impl.models;

import java.io.File;
import java.util.Collection;

import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.RDFSModelTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.vocabulary.SESAME;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class RDFSModelSesame2ImplTest extends RDFSModelTest {

	@BeforeClass
	public static void loadRepository() throws Exception {
		RDFSModelTest.initializeTest(new ARTModelFactorySesame2Impl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}

	protected void addTripleStoreSpecificNamedResources(Collection<ARTURIResource> expectedNamedResources) {
		expectedNamedResources.add(SESAME.Res.DIRECTSUBCLASSOF);
		expectedNamedResources.add(SESAME.Res.DIRECTSUBPROPERTYOF);
		expectedNamedResources.add(SESAME.Res.DIRECTTYPE);
	}
}
