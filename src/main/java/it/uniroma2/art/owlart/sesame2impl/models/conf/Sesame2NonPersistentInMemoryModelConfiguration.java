package it.uniroma2.art.owlart.sesame2impl.models.conf;

public class Sesame2NonPersistentInMemoryModelConfiguration extends Sesame2DirectAccessModelConfiguration
		implements Sesame2InMemoryModelConfiguration {

	public Sesame2NonPersistentInMemoryModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "in memory / no persist";
	}

}
