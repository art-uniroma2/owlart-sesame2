/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.query;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;

import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.sesame2impl.io.RDFFormatConverter;
import it.uniroma2.art.owlart.sesame2impl.navigation.Sesame2ARTStatementIteratorImpl;

public class GraphQuerySesame2Impl extends QuerySesame2Impl implements GraphQuery {

	public GraphQuerySesame2Impl(org.openrdf.query.GraphQuery query) {
		super(query);
	}

	public ARTStatementIterator evaluate(boolean infer) throws QueryEvaluationException {
		try {
			query.setIncludeInferred(infer);
			return new Sesame2ARTStatementIteratorImpl(((org.openrdf.query.GraphQuery) query).evaluate());
		} catch (org.openrdf.query.QueryEvaluationException e) {
			throw new QueryEvaluationException(e);
		}
	}

	public void evaluate(boolean infer, RDFFormat rdfFormat, OutputStream out)
			throws QueryEvaluationException, UnsupportedRDFFormatException, IOException {
		RDFWriter writer = RDFFormatConverter.getWriterFactory(rdfFormat).getWriter(out);
		try {
			query.setIncludeInferred(infer);
			((org.openrdf.query.GraphQuery) query).evaluate(writer);
		} catch (org.openrdf.query.QueryEvaluationException e) {
			throw new QueryEvaluationException(e);
		} catch (RDFHandlerException e) {
			throw new IOException(e);
		}
	}

	public void evaluate(boolean infer, RDFFormat rdfFormat, Writer out) throws QueryEvaluationException,
			UnsupportedRDFFormatException, IOException {
		RDFWriter writer = RDFFormatConverter.getWriterFactory(rdfFormat).getWriter(out);
		try {
			query.setIncludeInferred(infer);
			((org.openrdf.query.GraphQuery) query).evaluate(writer);
		} catch (org.openrdf.query.QueryEvaluationException e) {
			throw new QueryEvaluationException(e);
		} catch (RDFHandlerException e) {
			throw new IOException(e);
		}
	}

}
