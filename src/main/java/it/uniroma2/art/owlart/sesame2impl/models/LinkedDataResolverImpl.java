/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.models;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;

import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.util.RDFLoader;
import org.openrdf.rio.ParserConfig;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.openrdf.rio.helpers.RDFHandlerBase;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.LinkedDataResolver;
import it.uniroma2.art.owlart.sesame2impl.Sesame2ARTResourceFactory;

/**
 * Implementation of {@link LinkedDataResolver} for Sesame2.
 * 
 * <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class LinkedDataResolverImpl implements LinkedDataResolver {

	private ValueFactoryImpl valueFactory;
	private ParserConfig parserConfig;
	private Sesame2ARTResourceFactory ses2artFact;

	public LinkedDataResolverImpl() {
		this.valueFactory = ValueFactoryImpl.getInstance();
		this.ses2artFact = new Sesame2ARTResourceFactory(this.valueFactory);
		this.parserConfig = new ParserConfig();
	}

	public ValueFactory getValueFactory() {
		return valueFactory;
	}

	public ParserConfig getParserConfig() {
		return parserConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.LinkedDataResolver#lookup(it.uniroma2.art.owlart.model.ARTURIResource)
	 */
	@Override
	public Collection<ARTStatement> lookup(ARTURIResource resource) throws ModelAccessException,
			MalformedURLException, IOException {
		// Implementation based on org.openrdf.repository.base.RepositoryConnectionBase
		RDFLoader rdfLoader = new RDFLoader(getParserConfig(), getValueFactory());

		ARTContextStatementCollectorSesame2 rdfHandler = new ARTContextStatementCollectorSesame2();

		try {
			rdfLoader.load(new URL(resource.getNominalValue()), null, null, rdfHandler);
		} catch (RDFParseException e) {
			throw new ModelAccessException(e);
		} catch (RDFHandlerException e) {
			throw new ModelAccessException();
		} catch (UnsupportedRDFormatException e) {
			throw new IOException(e);
		}

		return rdfHandler.getStatements();
	}

	/**
	 * This class is an implementation of {@link RDFHandlerBase} that collects statements into a collection of
	 * {@link ARTStatement}s.
	 */
	private class ARTContextStatementCollectorSesame2 extends RDFHandlerBase {

		private Collection<ARTStatement> statements;

		public ARTContextStatementCollectorSesame2() {
			this.statements = new HashSet<ARTStatement>();
		}

		@Override
		public void handleStatement(Statement stat) throws RDFHandlerException {

			this.statements.add(ses2artFact.sesameStatement2ARTStatement(stat));
		}

		public Collection<ARTStatement> getStatements() {
			return statements;
		}
	}
}
