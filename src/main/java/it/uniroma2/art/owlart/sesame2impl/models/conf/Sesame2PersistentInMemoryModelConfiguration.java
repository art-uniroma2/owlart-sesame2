package it.uniroma2.art.owlart.sesame2impl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;

public class Sesame2PersistentInMemoryModelConfiguration extends Sesame2DirectAccessModelConfiguration
		implements PersistenceModelConfiguration, Sesame2InMemoryModelConfiguration {

	@ModelConfigurationParameter(description = "time in milliseconds before model is persisted; default is 1000 ms")
	public long syncDelay = 1000L;

	public Sesame2PersistentInMemoryModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "in memory / persistent";
	}

	public boolean isPersistent() {
		return true;
	}

}
