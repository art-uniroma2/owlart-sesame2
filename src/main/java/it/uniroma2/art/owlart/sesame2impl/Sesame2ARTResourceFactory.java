/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.sesame2impl.model.ARTBNodeSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTLiteralSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTNodeSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTResourceSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTStatementSesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTURIResourceSesame2Impl;

import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;

/**
 * this class provides convenience methods for bidirectional conversion of Sesame2 RDF types and OWL ART RDF
 * types
 * 
 * @author Armando Stellato
 * 
 */
public class Sesame2ARTResourceFactory {

	protected ValueFactory vFact;

	public Sesame2ARTResourceFactory(ValueFactory vFact) {
		this.vFact = vFact;
	}

	// *********************
	// *** ART 2 SESAME ****
	// *********************

	/**
	 * converts an OWL ART {@link ARTStatement} to a Sesame 2 {@link Statement}
	 * 
	 * @param artStat
	 * @return
	 */
	public Statement aRTStatement2SesameStatement(ARTStatement artStat) {
		if (artStat == null)
			return null;
		if (artStat instanceof ARTStatementSesame2Impl)
			return ((ARTStatementSesame2Impl) artStat).getSesameStatement();
		else {
			Statement stat = ((ARTStatementSesame2Impl) artStat).getSesameStatement();
			return vFact.createStatement(stat.getSubject(), stat.getPredicate(), stat.getObject());
		}			
	}

	/**
	 * converts an OWL ART {@link ARTURIResource} to a Sesame2 {@link URI}
	 * 
	 * @param sTres
	 * @return
	 */
	public URI aRTURIResource2SesameURI(ARTURIResource sTres) {
		if (sTres == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTres == NodeFilters.ANY)
			return null;
		if (sTres instanceof ARTURIResourceSesame2Impl)
			return ((ARTURIResourceSesame2Impl) sTres).getSesameURI();
		return vFact.createURI(sTres.getURI());
	}

	/**
	 * converts an OWL ART {@link ARTResource} to a Sesame2 {@link Resource}
	 * 
	 * @param sTres
	 * @return
	 */
	public Resource aRTResource2SesameResource(ARTResource sTres) {
		if (sTres == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTres == NodeFilters.ANY)
			return null;
		if (sTres instanceof ARTResourceSesame2Impl)
			return ((ARTResourceSesame2Impl) sTres).getSesameResource();

		if (sTres.isBlank()) {
			return vFact.createBNode(sTres.asBNode().getID());
		}

		return vFact.createURI(sTres.asURIResource().getURI());
	}

	/**
	 * converts an OWL ART {@link ARTNode} to a Sesame2 {@link Value}
	 * 
	 * @param sTNode
	 * @return
	 */
	public Value aRTNode2SesameValue(ARTNode sTNode) {
		if (sTNode == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTNode == NodeFilters.ANY)
			return null;
		if (sTNode instanceof ARTNodeSesame2Impl)
			return ((ARTNodeSesame2Impl) sTNode).getSesameValue();

		if (sTNode.isBlank()) {
			return vFact.createBNode(sTNode.asBNode().getID());
		}
		if (sTNode.isLiteral()) {
			ARTLiteral lit = sTNode.asLiteral();
			ARTURIResource dt = lit.getDatatype();
			if (dt != null)
				return vFact.createLiteral(lit.getLabel(), vFact.createURI(dt.getURI()));
			else
				return vFact.createLiteral(lit.getLabel(), lit.getLanguage());
		}
		return vFact.createURI(sTNode.asURIResource().getURI());
	}

	/**
	 * converts an OWL ART {@link ARTLiteral} to a Sesame2 {@link Literal}
	 * 
	 * @param stLiteral
	 * @return
	 */
	public Literal aRTLiteral2SesameLiteral(ARTLiteral stLiteral) {
		if (stLiteral == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (stLiteral == NodeFilters.ANY)
			return null;
		if (stLiteral instanceof ARTLiteralSesame2Impl)
			return ((ARTLiteralSesame2Impl) stLiteral).getLiteral();
		if (stLiteral.getDatatype() != null)
			return vFact.createLiteral(stLiteral.getLabel(),
					vFact.createURI(stLiteral.getDatatype().getURI()));
		return vFact.createLiteral(stLiteral.getLabel(), stLiteral.getLanguage());
	}

	// *********************
	// *** SESAME 2 ART ****
	// *********************

	/**
	 * converts Sesame2 {@link Statement} to an OWL ART {@link ARTStatement}
	 * 
	 * @param stat
	 * @return
	 */
	public ARTStatement sesameStatement2ARTStatement(Statement stat) {
		if (stat == null)
			return null;
		return new ARTStatementSesame2Impl(stat);
	}

	/**
	 * converts a Sesame2 {@link Resource} to an OWL ART {@link ARTResource}
	 * 
	 * @param res
	 * @return
	 */
	public ARTResource sesameResource2ARTResource(Resource res) {
		if (res == null)
			return NodeFilters.ANY;
		else
			return new ARTResourceSesame2Impl(res);
	}

	/**
	 * converts a Sesame2 {@link URI} to an OWL ART {@link ARTURIResource}
	 * 
	 * @param res
	 * @return
	 */
	public ARTURIResource sesameURI2ARTURIResource(URI res) {
		if (res == null)
			return NodeFilters.ANY;
		else
			return new ARTURIResourceSesame2Impl(res);
	}

	/**
	 * converts a Sesame2 {@link Value} to an OWL ART {@link ARTNode}
	 * 
	 * @param val
	 * @return
	 */
	public ARTNode sesameValue2ARTNode(Value val) {
		if (val == null)
			return NodeFilters.ANY;
		else
			return new ARTNodeSesame2Impl(val);
	}

	/**
	 * converts a Sesame2 {@link Literal} to an OWL ART {@link ARTLiteral}
	 * 
	 * @param sesLiteral
	 * @return
	 */
	public ARTLiteral sesameLiteral2ARTLiteral(Literal sesLiteral) {
		if (sesLiteral == null)
			return NodeFilters.ANY;
		else
			return new ARTLiteralSesame2Impl(sesLiteral);
	}

	/**
	 * converts a Sesame2 {@link BNode} to an OWL ART {@link ARTBNode}
	 * 
	 * @param sesBNode
	 * @return
	 */
	public ARTBNode sesameBNode2ARTBNode(BNode sesBNode) {
		if (sesBNode == null)
			return NodeFilters.ANY;
		else
			return new ARTBNodeSesame2Impl(sesBNode);
	}

}
