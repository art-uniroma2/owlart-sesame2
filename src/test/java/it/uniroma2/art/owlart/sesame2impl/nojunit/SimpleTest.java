package it.uniroma2.art.owlart.sesame2impl.nojunit;

import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2DirectAccessModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.vocabulary.OWL;

public class SimpleTest {

	public static void main(String[] args) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException, ModelCreationException, ModelAccessException,
			ModelUpdateException, UnsupportedRDFFormatException, IOException {

		boolean useReasoner = true;

		ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
		// OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory
		// .createModelFactory(factImpl);
		Sesame2DirectAccessModelConfiguration modelConf =

		factImpl.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		if (!useReasoner) {
			modelConf.directTypeInference = false;
			modelConf.rdfsInference = false;
		}

		String baseuri = "http://pippo.txt";

		OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);
		OWLModel model = fact.loadOWLModel(baseuri, "./src/test/resources/testRepo", modelConf);

		System.out.println(model.getNamespacePrefixMapping());

		model.setBaseURI(baseuri);
		model.setDefaultNamespace(baseuri + "#");

		System.out.println(model.getNamespacePrefixMapping());

		ARTURIResource subject = model.createURIResource("http://subject");
		ARTURIResource object = model.createURIResource("http://object");
		model.addTriple(subject, OWL.Res.DIFFERENTFROM, object);

		model.writeRDF(model.listStatements(subject, NodeFilters.ANY, NodeFilters.ANY, true, NodeFilters.MAINGRAPH),
				RDFFormat.RDFXML, System.out);

	}

}
