/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.navigation;

import info.aduna.iteration.CloseableIteration;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.sesame2impl.model.ARTResourceSesame2Impl;

import org.openrdf.model.Resource;
import org.openrdf.repository.RepositoryException;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class Sesame2ARTResourceIteratorImpl extends RDFIteratorImpl<ARTResource> implements ARTResourceIterator {
	
	private CloseableIteration<? extends Resource, RepositoryException> resIt;
    
    public Sesame2ARTResourceIteratorImpl(CloseableIteration<? extends Resource, RepositoryException> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
        try {
            resIt.close();
        } catch (RepositoryException e) {
            throw new ModelAccessException(e);
        }		
	}

	public ARTResource getNext() throws ModelAccessException {
        try {
			return new ARTResourceSesame2Impl(resIt.next());
        } catch (RepositoryException e) {           
            throw new ModelAccessException(e);
        }
	}

	public boolean streamOpen() throws ModelAccessException {
        try {
			return resIt.hasNext();
		} catch (RepositoryException e) {			
			throw new ModelAccessException(e);
		}
	}
	
	public String toString() {
		return resIt.toString();
	}

}
