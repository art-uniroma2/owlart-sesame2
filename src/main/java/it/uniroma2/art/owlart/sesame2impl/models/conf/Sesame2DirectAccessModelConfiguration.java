package it.uniroma2.art.owlart.sesame2impl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;

public abstract class Sesame2DirectAccessModelConfiguration extends ModelConfigurationImpl implements
		Sesame2ModelConfiguration {

	@ModelConfigurationParameter(description = "true if the sesame2 repository has to support directType inference; defaults to true")
	public boolean directTypeInference = true;

	@ModelConfigurationParameter(description = "true if the sesame2 repository has to support RDFS inferencing; defaults to true")
	public boolean rdfsInference = true;

	public Sesame2DirectAccessModelConfiguration() {
		super();
	}

}
