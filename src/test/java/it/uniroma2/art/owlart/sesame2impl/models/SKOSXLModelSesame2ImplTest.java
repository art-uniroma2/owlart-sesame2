package it.uniroma2.art.owlart.sesame2impl.models;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.SKOSXLModelTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.utilities.transform.Sesame2ModelComparator;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class SKOSXLModelSesame2ImplTest extends SKOSXLModelTest {
	
	@BeforeClass
	public static void loadRepository() throws Exception {
		SKOSXLModelTest.initializeTest(new ARTModelFactorySesame2Impl(), new Sesame2ModelComparator());
	}

	@AfterClass
	public static void classTearDown() {
		try {			
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}

	}

}
