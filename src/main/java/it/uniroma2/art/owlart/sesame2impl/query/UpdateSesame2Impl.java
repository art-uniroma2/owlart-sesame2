/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.query;

import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.query.UpdateExecutionException;

import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.sesame2impl.Sesame2ARTResourceFactory;

public class UpdateSesame2Impl implements Update {
	protected org.openrdf.query.Update query;

	protected Sesame2ARTResourceFactory fact;

	public UpdateSesame2Impl(org.openrdf.query.Update query) {
		this.query = query;
		ValueFactoryImpl vf = new ValueFactoryImpl();
		fact = new Sesame2ARTResourceFactory(vf);
	}

	public void evaluate(boolean infer) throws QueryEvaluationException {
		try {
			query.setIncludeInferred(infer);

			((org.openrdf.query.Update) query).execute();
		} catch (UpdateExecutionException e) {
			throw new QueryEvaluationException(e);
		}
	}

	public void setBinding(String name, ARTNode node) {
		query.setBinding(name, fact.aRTNode2SesameValue(node));
	}

}
