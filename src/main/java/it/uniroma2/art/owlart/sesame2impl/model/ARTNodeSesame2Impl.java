/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (Sesame2 Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (Sesame2 Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (Sesame2 Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.sesame2impl.model;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;

import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.Value;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTNodeSesame2Impl implements ARTNode {

	Value node;

    public ARTNodeSesame2Impl(Value res) {
        this.node = res; 
    }
    
    /**
     * convenience method for extracting encapsulated Sesame2 {@link Value}
     * 
     * @return
     */
    public Value getSesameValue() {
    	return node;
    }
    
	public ARTURIResource asURIResource() {
		return new ARTURIResourceSesame2Impl((URI)node);
	}
	
	public ARTBNode asBNode() {
		return new ARTBNodeSesame2Impl((BNode)node);
	}
    
	public ARTResource asResource() {
		return new ARTResourceSesame2Impl((Resource)node);
	}

	public ARTLiteral asLiteral() {
		return new ARTLiteralSesame2Impl((Literal)node);
	}

	public boolean isBlank() {
		return (node instanceof BNode);
	}

	public boolean isLiteral() {
		return (node instanceof Literal);
	}

	public boolean isURIResource() {
		return (node instanceof URI);
	}

	public boolean isResource() {
		return (node instanceof Resource);
	}
    
	public String getNominalValue() {
		return node.stringValue();
	}
	
	public String toString() {
		return node.toString();
	}
	
    public boolean equals(Object o) {
    	
    	// System.err.println("inside equals of empty URI resource between: " + this + " and " + o);
    	
        if (this == o) {
            return true;
        }

        if (o instanceof ARTNode) {
            return toString().equals(o.toString());
        }
        
        return false;
    }
	
	public int hashCode() {
    	return node.hashCode();
    }
}
