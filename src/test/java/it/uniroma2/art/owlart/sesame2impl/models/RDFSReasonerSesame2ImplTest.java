package it.uniroma2.art.owlart.sesame2impl.models;

import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.RDFSReasonerTest;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2DirectAccessModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;

public class RDFSReasonerSesame2ImplTest extends RDFSReasonerTest<Sesame2ModelConfiguration> {

	public ARTModelFactorySesame2Impl getModelFactory() {
		return new ARTModelFactorySesame2Impl();
	}

	public Sesame2ModelConfiguration getNonReasoningModelConfiguration(
			ModelFactory<Sesame2ModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		Sesame2DirectAccessModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		modelConf.directTypeInference = false;
		modelConf.rdfsInference = false;
		return modelConf;
	}

	public Sesame2ModelConfiguration getReasoningModelConfiguration(
			ModelFactory<Sesame2ModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		// default configuration covers RDFS reasoning
		return factImpl.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
	}

}
