package it.uniroma2.art.owlart.sesame2impl.models;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.EqualsOfRDFResourcesTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class EqualsOfRDFResourcesSesame2ImplTest extends EqualsOfRDFResourcesTest  {

	@BeforeClass
	public static void loadRepository() throws Exception {
		EqualsOfRDFResourcesTest.initializeTest(new ARTModelFactorySesame2Impl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			memStoreFile.delete();
			System.out.println("classTearDown: repository file deleted");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("classTearDown: failed to close the repository");
		}
	}
	
}


