package it.uniroma2.art.owlart.sesame2impl.utilities.transform;

import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.utilites.transform.SKOSXL2SKOSConverterTest;

import org.junit.Before;

public class SKOSXL2SKOSSesame2Test extends SKOSXL2SKOSConverterTest {
	@Before
	public void initializeTest() throws Exception {
		ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();

		Sesame2NonPersistentInMemoryModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		modelConf.rdfsInference = true;

		Sesame2NonPersistentInMemoryModelConfiguration modelConf2 = factImpl
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		modelConf2.rdfsInference = false;

		Sesame2NonPersistentInMemoryModelConfiguration modelConf3 = factImpl
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		modelConf3.rdfsInference = false;

		initializeTest(new Sesame2ModelComparator(), factImpl, modelConf, modelConf2, modelConf3);
	}
}
