package it.uniroma2.art.owlart.sesame2impl.utilities.transform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.util.ModelUtil;

import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.sesame2impl.Sesame2ARTResourceFactory;
import it.uniroma2.art.owlart.sesame2impl.model.ARTStatementSesame2Impl;
import it.uniroma2.art.owlart.utilites.transform.ModelComparator;

public class Sesame2ModelComparator implements ModelComparator {
	
	private Sesame2ARTResourceFactory ses2artFact = new Sesame2ARTResourceFactory(ValueFactoryImpl.getInstance());

	public boolean equals(RDFModel firstModel, RDFModel secondModel) throws Exception {
		ARTStatementIterator it = firstModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		Set<Statement> firstSet = new HashSet<Statement>();
		while (it.hasNext()) {
			firstSet.add(((ARTStatementSesame2Impl)it.next()).getSesameStatement());
		}
		it.close();
		
		it = secondModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		Set<Statement> secondSet = new HashSet<Statement>();
		while (it.hasNext()) {
			secondSet.add(((ARTStatementSesame2Impl)it.next()).getSesameStatement());
		}
		it.close();
		
		return ModelUtil.equals(firstSet, secondSet);
	}
	
	public boolean equals(Collection<ARTStatement> firstModel, Collection<ARTStatement> secondModel) throws Exception {
		List<Statement> sesFirstModel = new ArrayList<>();
		for (ARTStatement s : firstModel) {
			sesFirstModel.add(ses2artFact.aRTStatement2SesameStatement(s));
		}
		
		List<Statement> sesSecondModel = new ArrayList<>();
		for (ARTStatement s : secondModel) {
			sesSecondModel.add(ses2artFact.aRTStatement2SesameStatement(s));
		}
		
		return ModelUtil.equals(sesFirstModel, sesSecondModel);
	}

}
