/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is SemanticTurkey.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about SemanticTurkey can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.sesame2impl;

import java.util.HashMap;

/**
 * this map is a multicolumned table in which each column is indexed. For each column, each entry is a key.
 * 
 * @deprecated i just needed a BidiMap... (i will anyway maintain it and conclude its development in case of
 *             need of multiple indexed maps on more than two columns)
 * 
 * @author Armando Stellato
 * 
 */
public class MultiIndexedMap {

	@SuppressWarnings("unused")
	private HashMap<?, ?>[] indexMaps;

	// private MultiMap contentMap;

	public MultiIndexedMap(int arity) {
		indexMaps = new HashMap[arity];
		// contentMap = new MultiHashMap();
	}

	Object get(Object key, int searchIndex, int retrieveIndex) {
		// maps[search]
		return new Object();
	}

	/*
	 * mettere metodi per:
	 * 
	 * prendere un array di oggetti specificando l'indice della mappa dove cercare e gli indici che si
	 * vogliono indietro
	 */

}
