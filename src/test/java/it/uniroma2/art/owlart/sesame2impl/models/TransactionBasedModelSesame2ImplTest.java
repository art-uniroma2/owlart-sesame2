package it.uniroma2.art.owlart.sesame2impl.models;

import java.io.File;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.TransactionBasedModelTest;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class TransactionBasedModelSesame2ImplTest extends TransactionBasedModelTest {

	@BeforeClass
	public static void loadRepository() throws Exception {
		TransactionBasedModelTest.initializeTest(new ARTModelFactorySesame2Impl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			memStoreFile.delete();
			System.out.println("classTearDown: repository file deleted");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("classTearDown: failed to close the repository");
		}
	}
	
}
