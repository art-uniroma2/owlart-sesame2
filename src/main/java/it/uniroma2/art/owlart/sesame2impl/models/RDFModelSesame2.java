package it.uniroma2.art.owlart.sesame2impl.models;

import org.openrdf.repository.RepositoryConnection;

public interface RDFModelSesame2 {

	public RepositoryConnection getSesame2RepositoryConnection();
	
}
